DAEMON_VERSION=$(shell cat VERSION)

CC=g++
PKGCONFIG=`pkg-config --cflags --libs liblog4cxx dbus-c++-1`
CFLAGS=-pedantic -Wall -W -g $(PKGCONFIG) -DDOUANE_VERSION=\"$(DAEMON_VERSION)\"
LDFLAGS=$(PKGCONFIG) -lboost_signals -lboost_system -lboost_filesystem -lboost_regex -lcrypto -lpthread

OBJ=src/freedesktop/desktop_file.o \
	src/freedesktop/desktop_files.o \
	src/dbus/dbus_server.o \
	src/dbus/douane.o \
	src/main.o \
	src/netlink_listener.o \
	src/netlink_message_handler.o \
	src/netlink_socket.o \
	src/network_activity.o \
	src/process.o \
	src/processes_manager.o \
	src/rule.o \
	src/rules_manager.o \
	src/socket.o \
	src/thread.o \
	src/tools.o

USR_BIN_PATH=/usr/bin
INSTALL=$(USR_BIN_PATH)/install -c
BINDIR=$(DESTDIR)/opt/douane
PIDSDIR=$(DESTDIR)/opt/douane/pids
EXEC=douaned
SYSTEM_V_PATH=$(DESTDIR)/etc/init.d/
SYSTEMD_PATH=$(DESTDIR)/lib/systemd/system/
DBUS_PATH=$(DESTDIR)/etc/dbus-1/system.d/

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $(EXEC) $(OBJ) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(OBJ) $(EXEC)

dbus:
	dbusxx-xml2cpp d-bus/org.zedroot.douane.xml --adaptor=org_zedroot_douane.h

install: $(EXEC)
	test -d $(BINDIR) || mkdir -p $(BINDIR)
	test -d $(SYSTEM_V_PATH) || mkdir -p $(SYSTEM_V_PATH)
	test -d $(DBUS_PATH) || mkdir -p $(DBUS_PATH)
	install -m 0555 $(EXEC) $(BINDIR)
	test -d $(SYSTEMD_PATH) || install -m 0755 init.d/douane $(SYSTEM_V_PATH)
	install -m 0644 dbus/org.zedroot.Douane.conf $(DBUS_PATH)
	# Reload the dbus config files
	killall -HUP dbus-daemon
	test -f $(USR_BIN_PATH)/$(EXEC) && rm -f $(USR_BIN_PATH)/$(EXEC) || true
	ln -s $(BINDIR)/$(EXEC) $(USR_BIN_PATH)
	test -d $(SYSTEMD_PATH) && install -m 0644 systemd/douane-daemon.service $(SYSTEMD_PATH)
	test -d $(SYSTEMD_PATH) && systemctl daemon-reload
	test -d $(SYSTEMD_PATH) && systemctl enable douane-daemon
	test -d $(SYSTEMD_PATH) && systemctl start douane-daemon

uninstall: $(EXEC)
	# When using System V, stops the service and remove the service file
	test -d $(SYSTEMD_PATH) || $(SYSTEM_V_PATH)/douane stop
	test -d $(SYSTEMD_PATH) || rm -f $(SYSTEM_V_PATH)/douane
	# When using System D, stops the services, removes the files, and reload
	# systemd
	test -d $(SYSTEMD_PATH) && systemctl stop douane-daemon
	test -d $(SYSTEMD_PATH) && rm -rf $(SYSTEMD_PATH)/douane-daemon.service
	test -d $(SYSTEMD_PATH) && systemctl daemon-reload
	# Removes daemon files
	rm -f $(BINDIR)/$(EXEC)
	rm -f $(DBUS_PATH)/org.zedroot.Douane.conf
	# Reload the dbus config files
	killall -HUP dbus-daemon
	rm -rf $(BINDIR)
